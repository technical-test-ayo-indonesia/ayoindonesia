-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 28, 2020 at 07:43 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.3.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ayoindonesia`
--
CREATE DATABASE IF NOT EXISTS `ayoindonesia` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `ayoindonesia`;

-- --------------------------------------------------------

--
-- Table structure for table `anggota_tim`
--

CREATE TABLE `anggota_tim` (
  `anggotatim_id` bigint(20) NOT NULL,
  `is_active` bit(1) DEFAULT NULL,
  `beratbadan` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `nopunggung` int(11) NOT NULL,
  `posisi` varchar(255) NOT NULL,
  `tinggibadan` int(11) NOT NULL,
  `tim` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `anggota_tim`
--

INSERT INTO `anggota_tim` (`anggotatim_id`, `is_active`, `beratbadan`, `nama`, `nopunggung`, `posisi`, `tinggibadan`, `tim`) VALUES
(3, b'1', 80, 'Agung', 7, 'Penyerang', 170, 1),
(4, b'1', 74, 'Budi', 9, 'Gelandang', 168, 2);

-- --------------------------------------------------------

--
-- Table structure for table `hibernate_sequence`
--

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(9),
(9),
(9),
(9),
(9);

-- --------------------------------------------------------

--
-- Table structure for table `jadwal`
--

CREATE TABLE `jadwal` (
  `jadwal_id` bigint(20) NOT NULL,
  `is_active` bit(1) DEFAULT NULL,
  `tanggal` varchar(255) NOT NULL,
  `waktu` varchar(255) NOT NULL,
  `tim_tamu` bigint(20) NOT NULL,
  `tim_tuan` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `jadwal`
--

INSERT INTO `jadwal` (`jadwal_id`, `is_active`, `tanggal`, `waktu`, `tim_tamu`, `tim_tuan`) VALUES
(5, b'1', '12/01/2020', '13.00', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `papangoal`
--

CREATE TABLE `papangoal` (
  `ppn_goal_id` bigint(20) NOT NULL,
  `is_active` bit(1) DEFAULT NULL,
  `menitgoal` varchar(255) DEFAULT NULL,
  `jdwl_ppn_goal` bigint(20) NOT NULL,
  `ppn_skor_id` bigint(20) NOT NULL,
  `pemain_goal` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `papangoal`
--

INSERT INTO `papangoal` (`ppn_goal_id`, `is_active`, `menitgoal`, `jdwl_ppn_goal`, `ppn_skor_id`, `pemain_goal`) VALUES
(7, b'1', '24:52', 5, 6, 3),
(8, b'1', '63:26', 5, 6, 3);

-- --------------------------------------------------------

--
-- Table structure for table `papanskor`
--

CREATE TABLE `papanskor` (
  `ppn_skor_id` bigint(20) NOT NULL,
  `is_active` bit(1) DEFAULT NULL,
  `skortamu` int(11) NOT NULL,
  `skortuan` int(11) NOT NULL,
  `jadwal_skor` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `papanskor`
--

INSERT INTO `papanskor` (`ppn_skor_id`, `is_active`, `skortamu`, `skortuan`, `jadwal_skor`) VALUES
(6, b'1', 0, 2, 5);

-- --------------------------------------------------------

--
-- Table structure for table `tim`
--

CREATE TABLE `tim` (
  `tim_id` bigint(20) NOT NULL,
  `is_active` bit(1) DEFAULT NULL,
  `alamat` varchar(255) NOT NULL,
  `kota` varchar(255) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `tahun` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tim`
--

INSERT INTO `tim` (`tim_id`, `is_active`, `alamat`, `kota`, `logo`, `nama`, `tahun`) VALUES
(1, b'1', 'Arsenal street', 'London', 'Arsenal', 'Arsenal', 1984),
(2, b'1', 'Juventus street', 'Italia', 'Juventus', 'Juventus', 1982);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `anggota_tim`
--
ALTER TABLE `anggota_tim`
  ADD PRIMARY KEY (`anggotatim_id`),
  ADD KEY `FK1ftx042tnmws8x0yposmnpv61` (`tim`);

--
-- Indexes for table `jadwal`
--
ALTER TABLE `jadwal`
  ADD PRIMARY KEY (`jadwal_id`),
  ADD KEY `FKi1b406twhao8c0a7gi81k91l0` (`tim_tamu`),
  ADD KEY `FKdjb8ecrlkcj5e5vffi2398xwd` (`tim_tuan`);

--
-- Indexes for table `papangoal`
--
ALTER TABLE `papangoal`
  ADD PRIMARY KEY (`ppn_goal_id`),
  ADD KEY `FKkb19uav5vn0tuncniw29c5lsp` (`jdwl_ppn_goal`),
  ADD KEY `FKj0rxb1j39p0qws63507p6ali7` (`ppn_skor_id`),
  ADD KEY `FKe7y237j5gu0jcw234fo5t6dal` (`pemain_goal`);

--
-- Indexes for table `papanskor`
--
ALTER TABLE `papanskor`
  ADD PRIMARY KEY (`ppn_skor_id`),
  ADD KEY `FK2an4kqhj9fv2mrn3psfea7i5m` (`jadwal_skor`);

--
-- Indexes for table `tim`
--
ALTER TABLE `tim`
  ADD PRIMARY KEY (`tim_id`),
  ADD UNIQUE KEY `UK_cvbc2c3f73axefni20s4aac4f` (`nama`) USING HASH;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
