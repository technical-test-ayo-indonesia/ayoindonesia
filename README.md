![alt text](https://www.ayo.co.id/assets/logo/logotype.png)

### Persiapan
- Java 8
- Maven 3.6.0
- MySQL
- Postman

Aplikasi berjalan pada port 8080

## API TIM

``` 
GET :: localhost:8080/tim 

POST :: localhost:8080/tim
{
    "nama": "Arsenal",
    "logo": "Arsenal",
    "tahun": 1984,
    "alamat": "Arsenal street",
    "kota": "London"
}

PUT :: localhost:8080/tim/1
{
    "nama": "Arsenal",
    "logo": "Arsenal",
    "tahun": 1983,
    "alamat": "Arsenal street",
    "kota": "London"
}

DELETE :: localhost:8080/tim/1
```

## API Anggota Tim

``` 
GET :: :8080/anggota-tim

POST :: :8080/anggota-tim
{
    "tim": 1,
    "nama": "Agung",
    "tinggibadan": 170,
    "beratbadan": 80,
    "posisi": "Penyerang",
    "nopunggung": 7
}

PUT :: :8080/anggota-tim/3
{
    "tim": 1,
    "nama": "Agung",
    "tinggibadan": 170,
    "beratbadan": 80,
    "posisi": "Penyerang",
    "nopunggung": 14
}

DELETE :: :8080/anggota-tim/3
```

## API Jadwal

``` 
GET :: :8080/jadwal

POST :: :8080/jadwal
{
    "tanggal": "12/12/2020",
    "waktu": "13.00",
    "tuan": 1,
    "tamu": 2
}

PUT :: :8080/jadwal/3
{
    "tanggal": "12/12/2020",
    "waktu": "13.00",
    "tuan": 1,
    "tamu": 2
}

DELETE :: :8080/jadwal/3
```

## API Laporan

```
GET :: localhost:8080/papan-goal
```
