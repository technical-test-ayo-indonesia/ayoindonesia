package com.prasetyo.puji.ayoindonesia.controller;

import java.util.Optional;

import com.prasetyo.puji.ayoindonesia.entity.PapanGoal;
import com.prasetyo.puji.ayoindonesia.entity.PapanSkor;
import com.prasetyo.puji.ayoindonesia.repo.PapanGoalRepo;
import com.prasetyo.puji.ayoindonesia.repo.PapanSkorRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/papan-goal")
public class PapanGoalCon {

    @Autowired
    PapanGoalRepo papanGoalRepo;

    @Autowired
    PapanSkorRepo papanSkorRepo;

    @CrossOrigin
    @GetMapping("")
    ResponseEntity<Iterable<PapanGoal>> getAll(
        @RequestParam(required = false, defaultValue = "id") String col,
        @RequestParam(required = false) String sort,
        @RequestParam(required = false, defaultValue = "0") int page,
        @RequestParam(required = false, defaultValue = "5") int limit) {
        try {
            Sort sortby = (sort == null || sort.equalsIgnoreCase("DESC")) ?  Sort.by(col).descending() 
                : Sort.by(col).ascending();
            Pageable sortedById = PageRequest.of(page, limit, sortby);
            Page<PapanGoal> result = papanGoalRepo.findAll(sortedById);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @CrossOrigin
    @PostMapping("")
    ResponseEntity<PapanGoal> createOne(@RequestBody PapanGoal papanGoal) {
        try {
            Optional<PapanSkor> resultSkor = this.papanSkorRepo.findByJadwal(papanGoal.getJadwal());
            papanGoal.setLaporanskor(resultSkor.get());
            papanGoal.setActive(true);
            return new ResponseEntity<>(this.papanGoalRepo.save(papanGoal), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @CrossOrigin
    @PutMapping("/{id}")
    ResponseEntity<PapanGoal> updateOne(@RequestBody PapanGoal papanGoal, @PathVariable Long id) {
        try {
            Optional<PapanGoal> result = this.papanGoalRepo.findById(id);
            if (result.isPresent()) {
                Optional<PapanSkor> resultSkor = this.papanSkorRepo.findByJadwal(papanGoal.getJadwal());
                result.get().setJadwal(papanGoal.getJadwal());
                result.get().setLaporanskor(resultSkor.get());
                result.get().setPemain(papanGoal.getPemain());
                result.get().setMenitgoal(papanGoal.getMenitgoal());
                this.papanGoalRepo.save(result.get());
                return new ResponseEntity<>(result.get(), HttpStatus.CREATED);
            } else {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @CrossOrigin
    @DeleteMapping("/{id}")
    ResponseEntity<PapanGoal> deleteOne(@PathVariable Long id) {
        try {
            Optional<PapanGoal> team = this.papanGoalRepo.findById(id);
            if (team.isPresent()) {
                team.get().setActive(false);
                this.papanGoalRepo.save(team.get());
                return new ResponseEntity<>(team.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}