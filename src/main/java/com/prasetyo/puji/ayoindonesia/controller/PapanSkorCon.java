package com.prasetyo.puji.ayoindonesia.controller;

import java.util.Optional;

import com.prasetyo.puji.ayoindonesia.entity.PapanSkor;
import com.prasetyo.puji.ayoindonesia.repo.PapanSkorRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/papan-skor")
public class PapanSkorCon {

    @Autowired
    PapanSkorRepo papanSkorRepo;

    @CrossOrigin
    @GetMapping("")
    ResponseEntity<Iterable<PapanSkor>> getAll(
        @RequestParam(required = false, defaultValue = "id") String col,
        @RequestParam(required = false) String sort,
        @RequestParam(required = false, defaultValue = "0") int page,
        @RequestParam(required = false, defaultValue = "5") int limit) {
        try {
            Sort sortby = (sort == null || sort.equalsIgnoreCase("DESC")) ?  Sort.by(col).descending() 
                : Sort.by(col).ascending();
            Pageable sortedById = PageRequest.of(page, limit, sortby);
            Page<PapanSkor> result = papanSkorRepo.findAll(sortedById);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @CrossOrigin
    @PostMapping("")
    ResponseEntity<PapanSkor> createOne(@RequestBody PapanSkor papanSkor) {
        try {
            papanSkor.setActive(true);
            return new ResponseEntity<>(this.papanSkorRepo.save(papanSkor), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @CrossOrigin
    @PutMapping("/{id}")
    ResponseEntity<PapanSkor> updateOne(@RequestBody PapanSkor papanSkor, @PathVariable Long id) {
        try {
            Optional<PapanSkor> result = this.papanSkorRepo.findById(id);
            if (result.isPresent()) {
                result.get().setJadwal(papanSkor.getJadwal());
                result.get().setSkortuan(papanSkor.getSkortuan());
                result.get().setSkortamu(papanSkor.getSkortamu());
                this.papanSkorRepo.save(result.get());
                return new ResponseEntity<>(result.get(), HttpStatus.CREATED);
            } else {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @CrossOrigin
    @DeleteMapping("/{id}")
    ResponseEntity<PapanSkor> deleteOne(@PathVariable Long id) {
        try {
            Optional<PapanSkor> team = this.papanSkorRepo.findById(id);
            if (team.isPresent()) {
                team.get().setActive(false);
                this.papanSkorRepo.save(team.get());
                return new ResponseEntity<>(team.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}