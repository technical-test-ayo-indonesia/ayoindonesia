package com.prasetyo.puji.ayoindonesia.controller;

import java.util.Optional;

import com.prasetyo.puji.ayoindonesia.entity.Jadwal;
import com.prasetyo.puji.ayoindonesia.entity.Tim;
import com.prasetyo.puji.ayoindonesia.repo.JadwalRepo;
import com.prasetyo.puji.ayoindonesia.repo.TimRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/jadwal")
public class JadwalCon {

    @Autowired
    JadwalRepo jadwalRepo;

    @Autowired
    TimRepo timRepo;

    @CrossOrigin
    @GetMapping("")
    ResponseEntity<Iterable<Jadwal>> getAll(
        @RequestParam(required = false, defaultValue = "id") String col,
        @RequestParam(required = false) String sort,
        @RequestParam(required = false, defaultValue = "0") int page,
        @RequestParam(required = false, defaultValue = "5") int limit) {
        try {
            Sort sortby = (sort == null || sort.equalsIgnoreCase("DESC")) ?  Sort.by(col).descending() 
                : Sort.by(col).ascending();
            Pageable sortedById = PageRequest.of(page, limit, sortby);
            Page<Jadwal> result = jadwalRepo.findAll(sortedById);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @CrossOrigin
    @PostMapping("")
    ResponseEntity<Jadwal> createOne(@RequestBody Jadwal jadwal) {
        try {
            Optional<Tim> resultTuan = this.timRepo.findById(jadwal.getTuan().getId());
            Optional<Tim> resultTamu = this.timRepo.findById(jadwal.getTamu().getId());
            jadwal.setTuan(resultTuan.get());
            jadwal.setTamu(resultTamu.get());
            jadwal.setActive(true);
            return new ResponseEntity<>(this.jadwalRepo.save(jadwal), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @CrossOrigin
    @PutMapping("/{id}")
    ResponseEntity<Jadwal> updateOne(@RequestBody Jadwal jadwal, @PathVariable Long id) {
        try {
            Optional<Jadwal> result = this.jadwalRepo.findById(id);
            if (result.isPresent()) {
                Optional<Tim> resultTuan = this.timRepo.findById(jadwal.getTuan().getId());
                Optional<Tim> resultTamu = this.timRepo.findById(jadwal.getTamu().getId());
                result.get().setTanggal(jadwal.getTanggal());
                result.get().setWaktu(jadwal.getWaktu());
                result.get().setTuan(resultTuan.get());
                result.get().setTamu(resultTamu.get());
                this.jadwalRepo.save(result.get());
                return new ResponseEntity<>(result.get(), HttpStatus.CREATED);
            } else {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @CrossOrigin
    @DeleteMapping("/{id}")
    ResponseEntity<Jadwal> deleteOne(@PathVariable Long id) {
        try {
            Optional<Jadwal> team = this.jadwalRepo.findById(id);
            if (team.isPresent()) {
                team.get().setActive(false);
                this.jadwalRepo.save(team.get());
                return new ResponseEntity<>(team.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}