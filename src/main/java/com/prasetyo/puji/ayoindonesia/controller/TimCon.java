package com.prasetyo.puji.ayoindonesia.controller;

import java.util.Optional;

import com.prasetyo.puji.ayoindonesia.entity.Tim;
import com.prasetyo.puji.ayoindonesia.repo.TimRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/tim")
public class TimCon {

    @Autowired
    TimRepo timRepo;

    @CrossOrigin
    @GetMapping("")
    ResponseEntity<Iterable<Tim>> getAll(
        @RequestParam(required = false, defaultValue = "id") String col,
        @RequestParam(required = false) String sort,
        @RequestParam(required = false, defaultValue = "0") int page,
        @RequestParam(required = false, defaultValue = "5") int limit) {
        try {
            Sort sortby = (sort == null || sort.equalsIgnoreCase("DESC")) ?  Sort.by(col).descending() 
                : Sort.by(col).ascending();
            Pageable sortedById = PageRequest.of(page, limit, sortby);
            Page<Tim> result = timRepo.findAll(sortedById);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @CrossOrigin
    @PostMapping("")
    ResponseEntity<Tim> createOne(@RequestBody Tim tim) {
        try {
            Optional<Tim> team = this.timRepo.findByNama(tim.getNama());
            if (team.isPresent()) {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            } else {
                tim.setActive(true);
                return new ResponseEntity<>(this.timRepo.save(tim), HttpStatus.CREATED);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @CrossOrigin
    @PutMapping("/{id}")
    ResponseEntity<Tim> updateOne(@RequestBody Tim tim, @PathVariable Long id) {
        try {
            Optional<Tim> team = this.timRepo.findById(id);
            if (team.isPresent()) {
                team.get().setNama(tim.getNama());
                team.get().setLogo(tim.getLogo());
                team.get().setTahun(tim.getTahun());
                team.get().setAlamat(tim.getAlamat());
                team.get().setKota(tim.getKota());
                this.timRepo.save(team.get());
                return new ResponseEntity<>(team.get(), HttpStatus.CREATED);
            } else {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @CrossOrigin
    @DeleteMapping("/{id}")
    ResponseEntity<Tim> deleteOne(@PathVariable Long id) {
        try {
            Optional<Tim> team = this.timRepo.findById(id);
            if (team.isPresent()) {
                team.get().setActive(false);
                this.timRepo.save(team.get());
                return new ResponseEntity<>(team.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
