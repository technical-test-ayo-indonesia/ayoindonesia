package com.prasetyo.puji.ayoindonesia.controller;

import java.util.Optional;

import com.prasetyo.puji.ayoindonesia.entity.AnggotaTim;
import com.prasetyo.puji.ayoindonesia.entity.Tim;
import com.prasetyo.puji.ayoindonesia.repo.AnggotaTimRepo;
import com.prasetyo.puji.ayoindonesia.repo.TimRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/anggota-tim")
public class AnggotaTimCon {

    @Autowired
    AnggotaTimRepo anggotaTimRepo;

    @Autowired
    TimRepo timRepo;

    @CrossOrigin
    @GetMapping("")
    ResponseEntity<Iterable<AnggotaTim>> getAll(
        @RequestParam(required = false, defaultValue = "id") String col,
        @RequestParam(required = false) String sort,
        @RequestParam(required = false, defaultValue = "0") int page,
        @RequestParam(required = false, defaultValue = "5") int limit) {
        try {
            Sort sortby = (sort == null || sort.equalsIgnoreCase("DESC")) ?  Sort.by(col).descending() 
                : Sort.by(col).ascending();
            Pageable sortedById = PageRequest.of(page, limit, sortby);
            Page<AnggotaTim> result = anggotaTimRepo.findAll(sortedById);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @CrossOrigin
    @PostMapping("")
    ResponseEntity<AnggotaTim> createOne(@RequestBody AnggotaTim tim) {
        try {
            Optional<Tim> result = this.timRepo.findById(tim.getTim().getId());
            tim.setTim(result.get());
            tim.setActive(true);
            return new ResponseEntity<>(this.anggotaTimRepo.save(tim), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @CrossOrigin
    @PutMapping("/{id}")
    ResponseEntity<AnggotaTim> updateOne(@RequestBody AnggotaTim tim, @PathVariable Long id) {
        try {
            Optional<AnggotaTim> result = this.anggotaTimRepo.findById(id);
            if (result.isPresent()) {
                Optional<Tim> team = this.timRepo.findById(tim.getTim().getId());
                result.get().setTim(team.get());
                result.get().setNama(tim.getNama());
                result.get().setTinggiBadan(tim.getTinggiBadan());
                result.get().setBeratBadan(tim.getBeratBadan());
                result.get().setPosisi(tim.getPosisi());
                result.get().setNoPunggung(tim.getNoPunggung());
                this.anggotaTimRepo.save(result.get());
                return new ResponseEntity<>(result.get(), HttpStatus.CREATED);
            } else {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @CrossOrigin
    @DeleteMapping("/{id}")
    ResponseEntity<AnggotaTim> deleteOne(@PathVariable Long id) {
        try {
            Optional<AnggotaTim> team = this.anggotaTimRepo.findById(id);
            if (team.isPresent()) {
                team.get().setActive(false);
                this.anggotaTimRepo.save(team.get());
                return new ResponseEntity<>(team.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}