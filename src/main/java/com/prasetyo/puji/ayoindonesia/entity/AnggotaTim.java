package com.prasetyo.puji.ayoindonesia.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

@Entity
@Table(name = "ANGGOTA_TIM")
@Where(clause = "is_active=1")
public class AnggotaTim {
    @Id
    @Column(name = "anggotatim_id")
    @GeneratedValue(strategy = javax.persistence.GenerationType.AUTO)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "tim", nullable = false)
    private Tim tim;

    @Column(nullable = false)
    private String nama;

    @Column(nullable = false)
    private int tinggibadan;

    @Column(nullable = false)
    private int beratbadan;

    @Column(nullable = false)
    private String posisi;

    @Column(nullable = false)
    private int nopunggung;

    @Column(name = "is_active")
    private Boolean active;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "pemain")
    private Set<PapanGoal> papaGoal;

    public AnggotaTim() {
    }

    public AnggotaTim(Long id) {
        this.id = id;
    }

    public AnggotaTim(Long id, Tim tim, String nama, int tinggibadan, int beratbadan, String posisi, int nopunggung,
            Boolean active) {
        this.id = id;
        this.tim = tim;
        this.nama = nama;
        this.tinggibadan = tinggibadan;
        this.beratbadan = beratbadan;
        this.posisi = posisi;
        this.nopunggung = nopunggung;
        this.active = active;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Tim getTim() {
        return this.tim;
    }

    public void setTim(Tim tim) {
        this.tim = tim;
    }

    public String getNama() {
        return this.nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getTinggiBadan() {
        return this.tinggibadan;
    }

    public void setTinggiBadan(int tinggibadan) {
        this.tinggibadan = tinggibadan;
    }

    public int getBeratBadan() {
        return this.beratbadan;
    }

    public void setBeratBadan(int beratbadan) {
        this.beratbadan = beratbadan;
    }

    public String getPosisi() {
        return this.posisi;
    }

    public void setPosisi(String posisi) {
        this.posisi = posisi;
    }

    public int getNoPunggung() {
        return this.nopunggung;
    }

    public void setNoPunggung(int nopunggung) {
        this.nopunggung = nopunggung;
    }

    public Boolean getActive() {
        return this.active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}