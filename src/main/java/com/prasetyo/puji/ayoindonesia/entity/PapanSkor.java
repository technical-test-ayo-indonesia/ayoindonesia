package com.prasetyo.puji.ayoindonesia.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

@Entity
@Table(name = "PAPANSKOR")
@Where(clause = "is_active=1")
public class PapanSkor {
    @Id
    @Column(name = "ppn_skor_id")
    @GeneratedValue(strategy = javax.persistence.GenerationType.AUTO)
    private Long id;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "jadwal_skor", nullable = false)
    private Jadwal jadwal;

    private int skortuan;
    private int skortamu;

    @Column(name = "is_active")
    private Boolean active;

    public PapanSkor() {
    }

    public PapanSkor(Long id, Jadwal jadwal, int skortuan, int skortamu, Boolean active) {
        this.id = id;
        this.jadwal = jadwal;
        this.skortuan = skortuan;
        this.skortamu = skortamu;
        this.active = active;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Jadwal getJadwal() {
        return this.jadwal;
    }

    public void setJadwal(Jadwal jadwal) {
        this.jadwal = jadwal;
    }

    public int getSkortuan() {
        return this.skortuan;
    }

    public void setSkortuan(int skortuan) {
        this.skortuan = skortuan;
    }

    public int getSkortamu() {
        return this.skortamu;
    }

    public void setSkortamu(int skortamu) {
        this.skortamu = skortamu;
    }

    public Boolean getActive() {
        return this.active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

}