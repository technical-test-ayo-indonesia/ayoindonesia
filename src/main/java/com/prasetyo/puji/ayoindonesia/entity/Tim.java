package com.prasetyo.puji.ayoindonesia.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

@Entity
@Table(name = "TIM")
@Where(clause = "is_active=1")
public class Tim {
    @Id
    @Column(name = "tim_id")
    @GeneratedValue(strategy = javax.persistence.GenerationType.AUTO)
    private Long id;

    @Column(nullable = false, unique = true)
    private String nama;

    @Column(nullable = false)
    private String logo;

    @Column(nullable = false)
    private int tahun;

    @Column(nullable = false)
    private String alamat;

    @Column(nullable = false)
    private String kota;

    @Column(name = "is_active")
    private Boolean active;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "tim")
    private Set<AnggotaTim> anggotaTim;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "tuan")
    private Set<Jadwal> tuan;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "tamu")
    private Set<Jadwal> tamu;

    public Tim() {
    }

    public Tim(Long id) {
        this.id = id;
    }

    public Tim(Long id, String nama, String logo, int tahun, String alamat, String kota, Boolean active) {
        this.id = id;
        this.nama = nama;
        this.logo = logo;
        this.tahun = tahun;
        this.alamat = alamat;
        this.kota = kota;
        this.active = active;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNama() {
        return this.nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getLogo() {
        return this.logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public int getTahun() {
        return this.tahun;
    }

    public void setTahun(int tahun) {
        this.tahun = tahun;
    }

    public String getAlamat() {
        return this.alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getKota() {
        return this.kota;
    }

    public void setKota(String kota) {
        this.kota = kota;
    }

    public Boolean getActive() {
        return this.active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

}