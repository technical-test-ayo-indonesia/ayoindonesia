package com.prasetyo.puji.ayoindonesia.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

@Entity
@Table(name = "PAPANGOAL")
@Where(clause = "is_active=1")
public class PapanGoal {
    @Id
    @Column(name = "ppn_goal_id")
    @GeneratedValue(strategy = javax.persistence.GenerationType.AUTO)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "jdwl_ppn_goal", nullable = false)
    private Jadwal jadwal;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ppn_skor_id", nullable = false)
    private PapanSkor laporanskor;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "pemain_goal")
    private AnggotaTim pemain;

    private String menitgoal;

    @Column(name = "is_active")
    private Boolean active;

    public PapanGoal() {
    }

    public PapanGoal(Long id, Jadwal jadwal, PapanSkor laporanskor, AnggotaTim pemain, String menitgoal,
            Boolean active) {
        this.id = id;
        this.jadwal = jadwal;
        this.laporanskor = laporanskor;
        this.pemain = pemain;
        this.menitgoal = menitgoal;
        this.active = active;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Jadwal getJadwal() {
        return this.jadwal;
    }

    public void setJadwal(Jadwal jadwal) {
        this.jadwal = jadwal;
    }

    public PapanSkor getLaporanskor() {
        return this.laporanskor;
    }

    public void setLaporanskor(PapanSkor laporanskor) {
        this.laporanskor = laporanskor;
    }

    public AnggotaTim getPemain() {
        return this.pemain;
    }

    public void setPemain(AnggotaTim pemain) {
        this.pemain = pemain;
    }

    public String getMenitgoal() {
        return this.menitgoal;
    }

    public void setMenitgoal(String menitgoal) {
        this.menitgoal = menitgoal;
    }

    public Boolean getActive() {
        return this.active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

}