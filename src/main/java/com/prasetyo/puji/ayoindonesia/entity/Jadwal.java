package com.prasetyo.puji.ayoindonesia.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

@Entity
@Table(name = "JADWAL")
@Where(clause = "is_active=1")
public class Jadwal {
    @Id
    @Column(name = "jadwal_id")
    @GeneratedValue(strategy = javax.persistence.GenerationType.AUTO)
    private Long id;

    @Column(nullable = false)
    private String tanggal;

    @Column(nullable = false)
    private String waktu;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "tim_tuan", nullable = false)
    private Tim tuan;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "tim_tamu", nullable = false)
    private Tim tamu;

    @Column(name = "is_active")
    private Boolean active;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "jadwal", cascade = CascadeType.ALL)
    private Set<PapanGoal> papangoal;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "jadwal")
    private PapanSkor papanskor;

    public Jadwal() {
    }

    public Jadwal(Long id) {
        this.id = id;
    }

    public Jadwal(Long id, String tanggal, String waktu, Tim tuan, Tim tamu, Boolean active) {
        this.id = id;
        this.tanggal = tanggal;
        this.waktu = waktu;
        this.tuan = tuan;
        this.tamu = tamu;
        this.active = active;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTanggal() {
        return this.tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getWaktu() {
        return this.waktu;
    }

    public void setWaktu(String waktu) {
        this.waktu = waktu;
    }

    public Tim getTuan() {
        return this.tuan;
    }

    public void setTuan(Tim tuan) {
        this.tuan = tuan;
    }

    public Tim getTamu() {
        return this.tamu;
    }

    public void setTamu(Tim tamu) {
        this.tamu = tamu;
    }

    public Boolean getActive() {
        return this.active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}