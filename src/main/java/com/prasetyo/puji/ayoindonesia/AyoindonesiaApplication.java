package com.prasetyo.puji.ayoindonesia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AyoindonesiaApplication {

	public static void main(String[] args) {
		SpringApplication.run(AyoindonesiaApplication.class, args);
	}

}
