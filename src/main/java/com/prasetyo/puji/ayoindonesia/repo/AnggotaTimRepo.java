package com.prasetyo.puji.ayoindonesia.repo;

import com.prasetyo.puji.ayoindonesia.entity.AnggotaTim;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AnggotaTimRepo extends JpaRepository<AnggotaTim, Long> {
    Page<AnggotaTim> findAll(Pageable pageable);
}
