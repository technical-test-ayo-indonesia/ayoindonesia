package com.prasetyo.puji.ayoindonesia.repo;

import java.util.Optional;

import com.prasetyo.puji.ayoindonesia.entity.Jadwal;
import com.prasetyo.puji.ayoindonesia.entity.PapanSkor;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PapanSkorRepo extends JpaRepository<PapanSkor, Long> {
    Page<PapanSkor> findAll(Pageable pageable);

    Optional<PapanSkor> findByJadwal(Jadwal jadwal);
}