package com.prasetyo.puji.ayoindonesia.repo;

import com.prasetyo.puji.ayoindonesia.entity.Jadwal;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JadwalRepo extends JpaRepository<Jadwal, Long> {
    Page<Jadwal> findAll(Pageable pageable);
}