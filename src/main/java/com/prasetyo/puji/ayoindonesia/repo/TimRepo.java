package com.prasetyo.puji.ayoindonesia.repo;

import java.util.Optional;

import com.prasetyo.puji.ayoindonesia.entity.Tim;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TimRepo extends JpaRepository<Tim, Long> {
    Page<Tim> findAll(Pageable pageable);

    Optional<Tim> findByNama(String nama);
}