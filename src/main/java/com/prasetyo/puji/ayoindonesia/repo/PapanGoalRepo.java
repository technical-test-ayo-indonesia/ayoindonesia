package com.prasetyo.puji.ayoindonesia.repo;

import java.util.List;

import com.prasetyo.puji.ayoindonesia.entity.Jadwal;
import com.prasetyo.puji.ayoindonesia.entity.PapanGoal;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PapanGoalRepo extends JpaRepository<PapanGoal, Long> {
    Page<PapanGoal> findAll(Pageable pageable);

    List<PapanGoal> findByJadwal(Jadwal jadwal);
}